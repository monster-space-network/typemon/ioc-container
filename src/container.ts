import * as Inversify from 'inversify';
import { Newable } from '@typemon/types';
//
import { Identifier } from './types';
import { InjectionToken } from './injection-token';
import { BindingToSyntax } from './binding-to-syntax';
//
//
//
class Core extends Inversify.Container { }

export class Container {
    public static isContainer(value: unknown): value is Container {
        return value instanceof Container;
    }

    private readonly core: Core;
    private parentContainer: Container | null;

    public constructor() {
        this.core = new Core({ defaultScope: 'Singleton' });
        this.parentContainer = null;
    }

    public get parent(): Container | null {
        return this.parentContainer;
    }
    public set parent(container: Container | null) {
        this.parentContainer = container;
        this.core.parent = !!this.parentContainer ? this.parentContainer.core : null;
    }

    public createChild(): Container {
        const container: Container = new Container();

        container.parent = this;

        return container;
    }

    public isBound<Type>(identifier: Identifier<Type> | InjectionToken<Type>): boolean {
        if (InjectionToken.isInjectionToken(identifier)) {
            identifier = identifier.identifier;
        }

        return this.core.isBound(identifier);
    }
    public bind<Type>(identifier: Identifier<Type> | InjectionToken<Type>): BindingToSyntax<Type> {
        if (InjectionToken.isInjectionToken(identifier)) {
            identifier = identifier.identifier;
        }

        return new BindingToSyntax(this.core.bind(identifier));
    }
    public unbind<Type>(identifier: Identifier<Type> | InjectionToken<Type>): void {
        if (InjectionToken.isInjectionToken(identifier)) {
            identifier = identifier.identifier;
        }

        this.core.unbind(identifier);
    }

    public get<Type>(identifier: Identifier<Type> | InjectionToken<Type>): Type {
        if (InjectionToken.isInjectionToken(identifier)) {
            identifier = identifier.identifier;
        }

        return this.core.get(identifier);
    }

    public resolve<Type>(identifier: Newable<Type>): Type {
        return this.core.resolve(identifier);
    }

    public dispose(): void {
        this.parentContainer = null;
        this.core.unbindAll();
    }
}

export interface ReadonlyContainer {
    readonly parent: Container | null;
    get<Type>(identifier: Identifier<Type> | InjectionToken<Type>): Type;
}

export interface BindonlyContainer {
    readonly parent: Container | null;
    bind<Type>(identifier: Identifier<Type> | InjectionToken<Type>): BindingToSyntax<Type>;
    get<Type>(identifier: Identifier<Type> | InjectionToken<Type>): Type;
}
