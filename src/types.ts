import { Newable } from '@typemon/types';
//
//
//
export type Identifier<Type> = symbol | Newable<Type>;
export interface Identifiable<Type> {
    readonly identifier: Identifier<Type>;
}
