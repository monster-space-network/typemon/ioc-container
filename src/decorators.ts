import * as Inversify from 'inversify';
//
import { Identifier } from './types';
import { InjectionToken } from './injection-token';
//
//
//
export function Injectable(): ClassDecorator {
    return (target: Function): void => Inversify.injectable()(target);
}

export function Inject<Type>(identifier: Identifier<Type> | InjectionToken<Type>): ParameterDecorator {
    return (target: Object, propertyKey: string | symbol, parameterIndex: number): void => identifier instanceof InjectionToken
        ? Inversify.inject(identifier.identifier)(target, propertyKey as string, parameterIndex)
        : Inversify.inject(identifier)(target, propertyKey as string, parameterIndex);
}

export function Optional(): ParameterDecorator {
    return (target: Object, propertyKey: string | symbol, parameterIndex: number): void => Inversify.optional()(target, propertyKey as string, parameterIndex);
}
