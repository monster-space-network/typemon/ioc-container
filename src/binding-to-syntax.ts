import * as Inversify from 'inversify';
import { Newable } from '@typemon/types';
//
//
//
export class BindingToSyntax<Type> {
    public static isBindingToSyntax<Type>(value: unknown): value is BindingToSyntax<Type> {
        return value instanceof BindingToSyntax;
    }

    public constructor(
        private readonly bindingToSyntax: Inversify.interfaces.BindingToSyntax<Type>
    ) { }

    public to(value: Newable<Type>): void {
        this.bindingToSyntax.to(value);
    }

    public toSelf(): void {
        this.bindingToSyntax.toSelf();
    }

    public toConstantValue(value: Type): void {
        this.bindingToSyntax.toConstantValue(value);
    }

    public toDynamicValue(callback: () => Type): void {
        this.bindingToSyntax.toDynamicValue(callback);
    }
}
