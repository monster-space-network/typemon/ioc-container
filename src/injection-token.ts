import { Identifiable } from './types';
//
//
//
export class InjectionToken<Type> implements Identifiable<Type> {
    public static isInjectionToken<Type>(value: unknown): value is InjectionToken<Type> {
        return value instanceof InjectionToken;
    }

    public constructor(
        public readonly identifier: symbol
    ) { }
}
