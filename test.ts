import {
    /* Container Class and Interfaces */
    Container, ReadonlyContainer, BindonlyContainer,

    /* Binding Syntaxes */
    BindingToSyntax,

    /* Injection-Token Class */
    InjectionToken,

    /* Decorators */
    Injectable, Inject, Optional,

    /* Types */
    Identifiable, Identifier
} from './src';
//
//
//
const MonsterNameToken: InjectionToken<string> = new InjectionToken(Symbol.for('MonsterName'));
const MonsterName: () => ParameterDecorator = (): ParameterDecorator => Inject(MonsterNameToken);
const MonsterPackagesToken: InjectionToken<ReadonlyArray<string>> = new InjectionToken(Symbol.for('MonsterPackages'));
const MonsterPackages: () => ParameterDecorator = (): ParameterDecorator => Inject(MonsterPackagesToken);

@Injectable()
class MonsterService {
    public readonly packages: ReadonlyArray<string>;

    public constructor(
        @MonsterName() public readonly name: string,
        @MonsterPackages() @Optional() packages?: ReadonlyArray<string>
    ) {
        this.packages = packages || ['ioc-container'];
    }
}

@Injectable()
class Adventurer {
    public constructor(
        public readonly pet: MonsterService
    ) { }
}

const container: Container = new Container();

container.bind(MonsterNameToken).toConstantValue('Typemon');
container.bind(MonsterService).toSelf();
container.bind(Adventurer).toSelf();

const adventurer: Adventurer = container.resolve(Adventurer);

console.log(adventurer.pet.name, adventurer.pet.packages);
